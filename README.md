# Script instalar [Odoo](https://www.odoo.com "Odoo's Homepage") 11 para ubuntu 16.04 & 18.04
###### con las dependencias y modulos nesesarios para facturacion electronica CHILE.

los modulos que se instalan son los siguientes:
* l10n_cl_chart_of_account de  [Konos](https://github.com/KonosCL)
* l10n_cl_dte_point_of_sale [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* l10n_cl_fe [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* payment_khipu [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* payment_webpay [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* reporting-engine
* Script instalacion basado en [Odoo](https://www.odoo.com "Odoo's Homepage") https://github.com/Yenthe666/InstallScript

Es super basico el script basicamente ejectuta otro script basado en el de [Yenthe666] para instalacion odoo, clona los repositorios de los modulos mencionado e instala las dependecias para correecto funcionamiento de odoo y los modulos de facturacion electronica CHILENA
Ademas instala modulos para extender funcionalidades del POS
## Instalacion:

#### 1. bajar el script:
```
sudo wget https://gitlab.com/fanguloa/scripts_odoo11/raw/master/installscript.sh
```
#### 3. hacer ejecutable el script
```
sudo chmod +x installscript.sh
```
#### 4. Executar el script:
```
sudo ./installscript.sh
```

## Recuerden canmbiar Password en /etc/odoo-server.conf

sudo /etc/odoo-server.conf
     